$(document).ready(function() {

    $('#calendario-professor').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
        },
        navLinks: true, // can click day/week names to navigate views

        editable: true,
        events: [{
                title: 'Projeto Am',
                start: '2018-05-14',
                color: '#ff6383'
            },
            {
                title: 'Projeto Am',
                start: '2018-05-15',
                color: '#ff6383'
            },
            {
                title: 'Projeto Am',
                start: '2018-05-16',
                color: '#ff6383'
            },
            {
                title: 'Projeto Am',
                start: '2018-05-17',
                color: '#ff6383'
            },
            {
                title: 'Projeto Am',
                start: '2018-05-18',
                color: '#ff6383'
            },

            {
                title: 'PS Responsive Web Development',
                start: '2018-05-28',
            },
            {
                title: 'PS Domain Driven Design',
                start: '2018-05-29',
                color: '#76eaa7'
            },
            {
                title: 'PS Design de Software',
                start: '2018-05-30T19:00:00',
                color: '#d43030'
            }, {
                title: 'PS Computarional Thinking',
                start: '2018-05-31T19:00:00',
                color: '#f7dd26'
            },
        ]
    });


});