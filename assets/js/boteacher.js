var aboutOffset = $('#about').offset().top;
$(window).scroll(function() {
    var obj = $('.features')
    var yPos = -($(window).scrollTop() / obj.attr('data-speed'));
    var bgpos = '50% ' + yPos + 'px';
    obj.css('background-position', bgpos);

    var windScroll = $(window).scrollTop();

    if (windScroll >= aboutOffset) {
        $('header').addClass('dark')
    } else {
        $('header').removeClass('dark')
    }

});

$(document).ready(function() {

    $('.nav-link,.wrap-banner a').click(function(event) {
        event.preventDefault();
        $('.nav-link').removeClass("active");
        $(this).addClass("active");
        var divSelected = $(this).attr('href');
        console.log(divSelected);
        var divOffset = $(divSelected).offset().top;
        console.log(divOffset);
        $('html,body').animate({ scrollTop: divOffset }, 500);
    });

    $('#contact-form').on('submit', function(e) {
        e.preventDefault();
        var nome = $('input[name="nome"]');
        var email = $('input[name="email"]');
        var mensagem = $('#contact-form #mensagem');

        var regExp = RegExp('[a-zA-Z0-9.!#$%&’+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)$');
        var emailRegex = email.val();

        if (nome.val() == "") {
            alert("Preencha o campo nome!");
            nome.focus();
            return false;
        }

        if (emailRegex == "") {
            alert("Preencha o campo email!");
            email.focus();
            return false;
        }
        if (regExp.test(emailRegex) != true) {
            alert("E-mail inválido");
            email.focus();
            return false;
        }
        if (mensagem.val() == "") {
            alert("Preencha o campo mensagem!");
            mensagem.focus();
            return false;
        }
        alert("Sua mensagem foi enviada com sucesso!")
    });

});