$(document).ready(function() {
    //abre e fecha o BoTeacher
    $('.chat-header').click(function() {
        $('.chatbot').toggleClass('open');
    });

});



//editor de conteudo
$('#bold').on('click', function() {
    document.execCommand('bold', false, null);
});

$('#italic').on('click', function() {
    document.execCommand('italic', false, null);
});

$('#underline').on('click', function() {
    document.execCommand('underline', false, null);
});

$('#align-left').on('click', function() {
    document.execCommand('justifyLeft', false, null);
});

$('#align-center').on('click', function() {
    document.execCommand('justifyCenter', false, null);
});

$('#align-right').on('click', function() {
    document.execCommand('justifyRight', false, null);
});

$('#list-ul').on('click', function() {
    document.execCommand('insertUnorderedList', false, null);
});

$('#list-ol').on('click', function() {
    document.execCommand('insertOrderedList', false, null);
});

$('#fonts').on('change', function() {
    var font = $(this).val();
    document.execCommand('fontName', false, font);
});

$('#size').on('change', function() {
    var size = $(this).val();
    $('.editor').css('fontSize', size + 'px');
});


//constroi os gráficos de cada matéria
$('.materia[data-materia="computational-thinking"]').click(function(event) {

    $('.charts>div').fadeOut();
    $('.chartjs-hidden-iframe').remove();
    chartComputationalThinking();
    $('.charts .computational-thinking').fadeIn();

});

$('.materia[data-materia="design-software"]').click(function(event) {

    $('.charts>div').fadeOut();
    $('.chartjs-hidden-iframe').remove();

    chartDesignSoftware();
    $('.charts .design-software').fadeIn();

});

$('.materia[data-materia="domain-driven-design"]').click(function(event) {

    $('.charts>div').fadeOut();
    $('.chartjs-hidden-iframe').remove();
    chartDomainDrivenDesign();
    $('.charts .domain-driven-design').fadeIn();

});

$('.materia[data-materia="banco-de-dados"]').click(function(event) {

    $('.charts>div').fadeOut();
    $('.chartjs-hidden-iframe').remove();
    chartBancoDeDados();
    $('.charts .banco-de-dados').fadeIn();

});

$('.materia[data-materia="sustentabilidade"]').click(function(event) {

    $('.charts>div').fadeOut();
    $('.chartjs-hidden-iframe').remove();

    chartSustentabilidade();
    $('.charts .sustentabilidade').fadeIn();

});

$('.materia[data-materia="web"]').click(function(event) {

    $('.charts>div').fadeOut();
    $('.chartjs-hidden-iframe').remove();
    chartResponsiveWebDevelopment();
    $('.charts .web').fadeIn();

});

function chartComputationalThinking() {
    var ctx = document.getElementById("ct_chart").getContext('2d');
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["Comandos de decisão e repetição", "Vetores e repetição"],
            datasets: [{
                label: 'Nota',
                borderColor: '#19b798',
                data: [4, 10],
                fill: false
            }]
        },
        options: {}
    });
}

function chartDesignSoftware() {
    var dsc = document.getElementById("ds_chart").getContext('2d');
    var myLineChart = new Chart(dsc, {
        type: 'line',
        data: {
            labels: ["Design Thinking - IBM", "Modelos e Processos de Software"],
            datasets: [{
                label: 'Nota',
                borderColor: '#19b798',
                data: [3, 5],
                fill: false
            }]
        },
        options: {}
    });
}

function chartDomainDrivenDesign() {
    var ctx = document.getElementById("ddd_chart").getContext('2d');
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["Conteúdo Teórico - PPT´s - Questões objetivas", "Diagrama de Classe", "Herança e POO"],
            datasets: [{
                label: 'Nota',
                fill: false,
                borderColor: '#19b798',
                data: [6, 10, 8.5]
            }]
        },
        options: {}
    });
}

function chartBancoDeDados() {
    var ctx = document.getElementById("bd_chart").getContext('2d');
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["Modelo Entidade Relacionamento - NAC 1", " NAC 2 - Agregação, Herança, Recursão e Tipagem"],
            datasets: [{
                label: 'Nota',
                fill: false,
                borderColor: '#19b798',
                data: [8, 6]
            }]
        },
        options: {}
    });
}

function chartSustentabilidade() {
    var ctx = document.getElementById("fs_chart").getContext('2d');
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [" NACEAD Cap.1 - Mercado de TI ", "NACEAD Cap.2 - Sustentabilidade"],
            datasets: [{
                label: 'Nota',
                fill: false,
                borderColor: '#19b798',
                data: [4, 2]
            }]
        },
        options: {}
    });
}

function chartResponsiveWebDevelopment() {
    var ctx = document.getElementById("wb_chart").getContext('2d');
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["QUESTIONÁRIO 01 - HTML / CSS	", "ENTREGA SITES LAYOUT - HTML / CSS", "NAC - NAC 01 - ENTREGA HTML"],
            datasets: [{
                label: 'Nota',
                fill: false,
                borderColor: '#19b798',
                data: [10, 10, 10]
            }]
        },
        options: {}
    });
}